import React, { Component } from 'react';
import { View, StyleSheet,Image,TouchableOpacity,Text } from 'react-native';




export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
            <Image source={require('./images/logo.png')} style= {{width:200, height:70}} />
       </View>
       <View style={styles.logo}>
            <Text style={{paddingBottom:10}}>LOGIN</Text>
            <Text style={{paddingTop:10,paddingBottom:10, fontSize:11}}>Username/ Email</Text>
            <View style={styles.userpassword}/>
            <Text style={{paddingTop:10,paddingBottom:10, fontSize:11}}>Password</Text>
            <View style={styles.userpassword}/>
        </View>
        <View style={styles.button}>
            <TouchableOpacity style={styles.loginbutton}>
            <Text style={styles.logintittle}>LOGIN</Text>
            </TouchableOpacity>
            <Text style={{paddingTop:20,paddingBottom:20, fontSize:15}}>atau</Text>
            <TouchableOpacity style={styles.registerbutton}>
            <Text style={styles.logintittle}>DAFTAR</Text>
            </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo:{
    flexDirection:'column',
    justifyContent:'space-around',
    paddingTop:100,
    alignItems:'center'
  },
  button:{
    flexDirection:'column',
    justifyContent:'space-around',
    paddingTop:35,
    alignItems:'center',
  },
  userpassword:{
    width: 250,
    height: 35,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 1,
    alignItems:'center',
    justifyContent:'center',
  },
  loginbutton:{
    width: 80,
    height: 25,
    backgroundColor: '#05838B',
    borderWidth: 2,
    borderColor: '#05838B',
    borderRadius: 8,
    alignItems:'center',
    justifyContent:'center'
  },
  registerbutton:{
    width: 80,
    height: 25,
    backgroundColor: 'green',
    borderWidth: 2,
    borderColor: 'green',
    borderRadius: 8,
    alignItems:'center',
    justifyContent:'center'
  },
  logintittle:{
    fontSize:11,
    color:'#FFFFFF',
  }
})
