// Soal 1
var i = 2;
console.log("LOOPING PERTAMA")
while(i<=20){
    if(i%2 == 0){
        console.log(`${i} - I love coding`)
    }
    i++;
}

console.log("LOOPING KEDUA")
while(i>=1){
    if(i%2 == 0){
        console.log(`${i} - I will become a mobile developer`)
    }
    i--;
}

// Soal 2
for(var i=1; i<=20; i++){
    if(i%2==1){
        if(i%3==0){
            console.log(`${i} - I Love Coding`);
        } else{
            console.log(`${i} - Santai`);
        }
    } else{
        console.log(`${i} - Berkualitas`);
    }
}

// Soal 3
for(var i=1; i<=4; i++){
    for(var j=1; j<=8; j++){
        process.stdout.write(`#`);
    }
    console.log("");
}

// Soal 4
for(var i=1; i<=7; i++){
    for(var j=1; j<=i; j++){
        process.stdout.write(`#`);
    }
    console.log("");
}

// Soal 5
for(var i=1; i<=8; i++){
    if(i%2==1){
        for(var j=1; j<=8; j++){
            if(j%2==1){
                process.stdout.write(` `);
            } else{
                process.stdout.write(`#`);
            }
        }
        console.log("");
    } else{
        for(var j=1; j<=8; j++){
            if(j%2==1){
                process.stdout.write(`#`);
            } else{
                process.stdout.write(` `);
            }
        }
        console.log("");
    }
}