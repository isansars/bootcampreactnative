import React, { Component } from 'react';
import { View, StyleSheet,Image,TouchableOpacity,Text,FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoitem';
import data from './data.json';



export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.boxatas} >
        <Image source={require('./images/logo.png')} style= {{width:98, height:22}} />
        <View style={styles.searchitem} >
        <TouchableOpacity>
        <Icon style={styles.accountitem} name='search' size={25} />
        </TouchableOpacity>
        <TouchableOpacity>
        <Icon style={styles.accountitem} name='account-circle' size={25} />
        </TouchableOpacity>
        </View>
        </View>
  
        <View style={styles.body}>
        <FlatList
        data={data.items}
        renderItem={(video)=><VideoItem video={video.item}/>}
        keyExtractor={(item)=>item.id}
        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
        />
        </View>
        
        <View style={styles.tabbar}>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='home' size={25} />
        <Text style={styles.tabtittle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='whatshot' size={25} />
        <Text style={styles.tabtittle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='subscriptions' size={25} />
        <Text style={styles.tabtittle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabitem}>
        <Icon name='folder' size={25} />
        <Text style={styles.tabtittle}>Library</Text>
        </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  boxatas: {
    height: 50,
    elevation:3,
    backgroundColor: 'white',
    paddingHorizontal:15,
    flexDirection:'row',
    alignItems:'center',
    justifyContent: 'space-between'
  },
  searchitem:{
    flexDirection:'row'

  },
  accountitem:{
    marginLeft:25
  },
  tabbar:{
    backgroundColor:'white',
    height:60,
    borderTopWidth:0.5,
    borderColor:'#E5E5E5',
    flexDirection:'row',
    justifyContent:'space-around'
  },
  body:{
    flex:1
  },
  tabitem:{
  alignItems:'center',
  justifyContent:'center'
  },
  tabtittle:{
    fontSize:11,
    color:'#3c3c3c',
    paddingTop:3
  }
})
