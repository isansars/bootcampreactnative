// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini

let waktu   = 10000;
let i       = 0;
function callReadBooks(){
    readBooks(waktu, books[i], function(sisaWaktu){
        waktu = sisaWaktu;
        i++;
        if (i == 3){
            i = 0;
        }
        if ((waktu > 0) && (waktu < books[i].timeSpent)){
            waktu -= waktu;
            callReadBooks();
        } else if (waktu == 0){
            return;
        } else{
            callReadBooks();
        }
    })
}

callReadBooks();