// Soal 1 (Range)
console.log(`============== Soal 1 - Range ==============`)
function range(startNum, finishNum){
    if ((startNum != null) && (finishNum != null)){
        var result = [];
        if(startNum <= finishNum){
            while(startNum <= finishNum){
                result.push(startNum);
                startNum++;
            }
        } else{
            while(startNum >= finishNum){
                result.push(startNum);
                startNum--;
            }
        }
        return result;
    } else{
        return -1;
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2 (Range with Step)
console.log(`============== Soal 2 - Range with Step ==============`)
function rangeWithStep(startNum, finishNum, step){
    if ((startNum != null) && (finishNum != null)){
        var result = [];
        if(startNum <= finishNum){
            while(startNum <= finishNum){
                result.push(startNum);
                startNum += step;
            }
        } else{
            while(startNum >= finishNum){
                result.push(startNum);
                startNum -= step;
            }
        }
        return result;
    } else{
        return -1;
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3 (Sum of Range)
console.log(`============== Soal 3 - Sum of Range ==============`)
function sum(startNum, finishNum, step){
    var arr     = [];
    let result  = 0;
    if ((finishNum != null) && (step != null)){
        arr = rangeWithStep(startNum, finishNum, step);
        for(var i=0; i<arr.length; i++){
            result += arr[i];
        }
    } else if (finishNum != null){
        arr = range(startNum, finishNum);
        for(var i=0; i<arr.length; i++){
            result += arr[i];
        }
    } else if (startNum != null){
        result += startNum;
    }
    return result;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal 4 (Array Multidimensi)
console.log(`============== Soal 4 - Array Multidimensi ==============`)
function dataHandling(input){
    var temp = [];
    var totalData = input.length; // 4
    for(var i=0; i<totalData; i++){
        temp = input[i];
        console.log(`Nomor ID:  ${temp[0]}`);
        console.log(`Nama Lengkap:  ${temp[1]}`);
        console.log(`TTL:  ${temp[2]} ${temp[3]}`);
        console.log(`Hobi:  ${temp[4]}`)
        console.log(``)
    }
    return ``;
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
console.log(dataHandling(input));

/*
    Nomor ID:  0001
    Nama Lengkap:  Roman Alamsyah
    TTL:  Bandar Lampung 21/05/1989
    Hobi:  Membaca
    
    Nomor ID:  0002
    Nama Lengkap:  Dika Sembiring
    TTL:  Medan 10/10/1992
    Hobi:  Bermain Gitar
    
    Nomor ID:  0003
    Nama Lengkap:  Winona
    TTL:  Ambon 25/12/1965
    Hobi:  Memasak
    
    Nomor ID:  0004
    Nama Lengkap:  Bintang Senjaya
    TTL:  Martapura 6/4/1970
    Hobi:  Berkebun 
*/

// Soal 5 (Balik Kata)
console.log(`============== Soal 5 - Balik Kata ==============`)
function balikKata(input){
    var result = [];
    var n = input.length;
    for(var i=0; i<n; i++)   {
        result[i] = input[n-1-i];
        process.stdout.write(result[i]);
    }
    return ``;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6 (Metode Array)
console.log(`============== Soal 6 - Metode Array ==============`)
/*
    //contoh input
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
*/
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
function dataHandling2(input){
    input.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(input);
    var TTL = input[3];
    var arrTTL = TTL.split("/");
    var bulan = arrTTL[1];
    var month = parseInt(bulan);
    switch(month){
        case 01: {month = "Januari"; break;}
        case 02: {month = "Februari"; break;}
        case 03: {month = "Maret"; break;}
        case 04: {month = "April"; break;}
        case 05: {month = "Mei"; break;}
        case 06: {month = "Juni"; break;}
        case 07: {month = "Juli"; break;}
        case 08: {month = "Agustus"; break;}
        case 09: {month = "September"; break;}
        case 10: {month = "Oktober"; break;}
        case 11: {month = "November"; break;}
        case 12: {month = "Desember"; break;}
        default: {month = "Bukan Bulan";}
    }
    console.log(month);
    var sortTTL = arrTTL.slice();
    sortTTL.sort(function (value1, value2){
        return value2 - value1 
    });
    console.log(sortTTL);
    TTL = arrTTL.join("-");
    console.log(TTL);
    var nama = input[1];
    var namaSlice = nama.slice(0, 14);
    console.log(namaSlice);
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);