import React, { Component } from 'react';
import { View, StyleSheet,Image,TouchableOpacity,Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class VideoItem extends Component {
render(){
let video= this.props.video;
return(
    <View style={styles.container}>
        <Image source={{uri:video.snippet.thumbnails.medium.url}} style={{height:200}}></Image>
        <View style={styles.desccontainer}>
            <Image source={{uri:'https://m.media-amazon.com/images/M/MV5BNDUwNjBkMmUtZjM2My00NmM4LTlmOWQtNWE5YTdmN2Y2MTgxXkEyXkFqcGdeQXRyYW5zY29kZS13b3JrZmxvdw@@._V1_UX477_CR0,0,477,268_AL_.jpg'}} style={{width:50, height:50,borderRadius:25}}></Image>
            <View style={styles.videodetail}>
                <Text numberOfLines={2} style={styles.videotitle}>{video.snippet.title}</Text>
                <Text style={styles.videostats}>{video.snippet.channelTitle+' · '+nFormatter(video.statistics.viewCount)+' · 3 months ago'}</Text>
            </View>
            <TouchableOpacity style={styles.tabitem}>
            <Icon name='more-vert' size={20} color='#999999'/>
            </TouchableOpacity>
        </View>
    </View>
)
}
}

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol+' views ';
  }

const styles = StyleSheet.create({
    container: {
      padding:15
    },
    desccontainer:{
        flexDirection:'row',
        paddingTop:15
    },
    videodetail:{
        paddingHorizontal:15,
        flex:1
    },
    videotitle:{
        fontSize:16,
        color:'#3c3c3c'
    },
    videostats:{
     fontSize:14  
    }
})
