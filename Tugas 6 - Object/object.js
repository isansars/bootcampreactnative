// Soal 1 (Array to Object)
console.log(`============== Soal 1 - Array to Object ==============`)
function arrayToObject(arr) {
    // Code di sini 
    var results = {}
    var now = new Date()
    var thisYear = now.getFullYear()
    var n = arr.length;
    for(var i=0; i<n; i++){
        var m = arr[i];
        var mlength = arr[i].length;
        var result = {
            firstName: "",
            lastName: "",
            gender: "",
            age: ""
        }
        var umur;
        result.firstName    = m[0];
        result.lastName     = m[1];
        result.gender       = m[2];
        if (mlength == 4){
            if (thisYear<m[3]){
                umur = "Invalid Birth Year"
            } else{
                umur = thisYear - m[3];
            }
            result.age          = umur;
        } else{
            result.age          = "Invalid Birth Year"
        }
        results[i] = result;
    }
    for(people in results){
        process.stdout.write(`${parseInt(people)+1}. ${results[people].firstName} ${results[people].lastName}: `);
        console.log(results[people])
    }
    console.log("")
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal 2 (Shopping Time)
console.log(`============== Soal 2 - Shopping Time ==============`)
/*
    Sepatu brand Stacattu seharga 1.500.000
    Baju brand Zoro seharga 500.000
    Baju brand H&N seharga 250.000
    Sweater brand Uniklooh seharga 175.000
    Casing Handphone seharga 50.000
*/
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var arrBarang = [];
    var result;
    var uangAwal = money;
    if ((memberId == null) || (memberId == "")){
        result = `Mohon maaf, toko X hanya berlaku untuk member saja`;
    } else if (money < 50000){
        result = `Mohon maaf, uang tidak cukup`;
    } else{
        while((money >= 50000) && (!arrBarang.includes("Casing Handphone"))){
            if((money >= 1500000) && (!arrBarang.includes("Sepatu Stacattu"))){
                money -= 1500000;
                arrBarang.push("Sepatu Stacattu");
            } else if((money >= 500000) && (!arrBarang.includes("Baju Zoro"))){
                money -= 500000;
                arrBarang.push("Baju Zoro");
            } else if((money >= 250000) && (!arrBarang.includes("Baju H&N"))){
                money -= 250000;
                arrBarang.push("Baju H&N");
            } else if((money >= 175000) && (!arrBarang.includes("Sweater Uniklooh"))){
                money -= 175000;
                arrBarang.push("Sweater Uniklooh");
            } else if((money >= 50000) && (!arrBarang.includes("Casing Handphone"))){
                money -= 50000;
                arrBarang.push("Casing Handphone");
            } 
        }
        result = {
            "memberId": memberId,
            "money": uangAwal,
            "listPurchased": arrBarang,
            "changeMoney":  money
        }
    }
    return result
}
    
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
    //{ memberId: '82Ku8Ma742',
    // money: 170000,
    // listPurchased:
    //  [ 'Casing Handphone' ],
    // changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3 (Naik Angkot)
console.log("")
console.log(`============== Soal 3 - Naik Angkot ==============`)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var results = [];
    var result = {};
    var jumlahRute, berangkat, destinasi;
    for(i in arrPenumpang){
        for(j in rute){
            if (arrPenumpang[i][1] == rute[j]){
                berangkat = j;
                break;
            }
        }
        for(j in rute){
            if (arrPenumpang[i][2] == rute[j]){
                destinasi = j;
                break;
            }
        }
        jumlahRute          = destinasi - berangkat;

        result.penumpang    = arrPenumpang[i][0];
        result.naikDari     = arrPenumpang[i][1];
        result.tujuan       = arrPenumpang[i][2];
        result.bayar        = jumlahRute*2000;
        results.push(Object.assign({}, result));
    }
    return results
}
    
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
    
console.log(naikAngkot([])); //[]