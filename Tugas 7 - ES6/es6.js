// Soal 1 (Mengubah fungsi menjadi fungsi arrow)
console.log(`============== Soal 1 - Mengubah fungsi menjadi fungsi arrow ==============`)
const golden = goldenFunction = () => {
    console.log("this is golden!!")
}
    
golden()
console.log()

// Soal 2 (Sederhanakan menjadi Object literal di ES6)
console.log(`============== Soal 2 - Sederhanakan menjadi Object literal di ES6 ==============`)
const newFunction = literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
            return 
        }
    }
}
    
//Driver Code 
newFunction("William", "Imoh").fullName() 
console.log()

// Soal 3 (Destructuring)
console.log(`============== Soal 3 - Destructuring ==============`)
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

let {firstName, lastName, destination, occupation} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)
console.log()

// Soal 4 (Array Spreading)
console.log(`============== Soal 4 - Array Spreading ==============`)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];


//Driver Code
console.log(combined)
console.log()

// Soal 5 (Template Literals)
console.log(`============== Soal 5 - Template Literals ==============`)
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam'`
 
// Driver Code
console.log(before) 
console.log()