var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise

let waktu   = 10000;
let i       = 0;
function callReadBooks(){
    readBooksPromise(waktu, books[i])
        .then(function(sisaWaktu){
            waktu = sisaWaktu;
            i++;
            if (i == 3){
                i = 0;
            }
            if ((waktu > 0) && (waktu < books[i].timeSpent)){
                waktu -= waktu;
                callReadBooks();
            } else{
                callReadBooks();
            }
        })
        .catch(function(sisaWaktu){
            return;
        })
}

callReadBooks();