
// If-Else
var nama = "Junaedi"
var peran = "Werewolf"

// // Output untuk Input nama = '' dan peran = ''
// "Nama harus diisi!"
 
// //Output untuk Input nama = 'John' dan peran = ''
// "Halo John, Pilih peranmu untuk memulai game!"
 
// //Output untuk Input nama = 'Jane' dan peran 'Penyihir'
// "Selamat datang di Dunia Werewolf, Jane"
// "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
 
// //Output untuk Input nama = 'Jenita' dan peran 'Guard'
// "Selamat datang di Dunia Werewolf, Jenita"
// "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
 
// //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
// "Selamat datang di Dunia Werewolf, Junaedi"
// "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"

if (nama == ""){
    console.log("Nama harus diisi!");
} else if (peran == ""){
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
} else {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    if (peran == "Penyihir"){
        console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
    } else if (peran == "Guard"){
        console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
    } else if (peran == "Werewolf"){
        console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
    }
}

// Switch-Case
var tanggal = 18; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 6; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2000; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var month = "";

switch(bulan){
    case 1: {month = "Januari"; break;}
    case 2: {month = "Februari"; break;}
    case 3: {month = "Maret"; break;}
    case 4: {month = "April"; break;}
    case 5: {month = "Mei"; break;}
    case 6: {month = "Juni"; break;}
    case 7: {month = "Juli"; break;}
    case 8: {month = "Agustus"; break;}
    case 9: {month = "September"; break;}
    case 10: {month = "Oktober"; break;}
    case 11: {month = "November"; break;}
    case 12: {month = "Desember"; break;}
    default: {month = "Januari";}
}

console.log(`${tanggal} ${month} ${tahun}`);